﻿CREATE TABLE [nilnul.acc_].[Norsc] --no rsc
(
	[id] bigINT NOT NULL PRIMARY KEY identity 
	,
	[name] nvarchar(400) 
	,
	[hash_salt] binary(64) --random number
	,
	[pass_hashed] BINARY(64) -- the hashed value;
	,
	_time datetime default getUtcDate() -- not getDate()
	,
	_memo nvarchar(max) 

)
