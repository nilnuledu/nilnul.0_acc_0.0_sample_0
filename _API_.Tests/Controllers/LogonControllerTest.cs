﻿using _API_.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace nilnul._acc_.API._TEST_.Controllers
{
	[TestClass]
	public class LogonControllerTest
	{
		[TestMethod]
		public void Get()
		{
			// Arrange
			ValuesController controller = new ValuesController();

			// Act
			IEnumerable<string> result = controller.Get();

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(2, result.Count());
			Assert.AreEqual("value1", result.ElementAt(0));
			Assert.AreEqual("value2", result.ElementAt(1));
		}

		[TestMethod]
		public void GetById()
		{
			// Arrange
			ValuesController controller = new ValuesController();

			// Act
			string result = controller.Get(5);

			// Assert
			Assert.AreEqual("value", result);
		}

		[TestMethod]
		public void Post()
		{
			// Arrange
			var controller = new nilnul._acc_._API_.Controllers.LogonController();

			// Act
			var tok=controller.Post("z6","z6");

			Debug.WriteLine(tok?.ToString());

			// Assert


		}

		[TestMethod]
		public void Put()
		{
			// Arrange
			ValuesController controller = new ValuesController();

			// Act
			controller.Put(5, "value");

			// Assert
		}

		[TestMethod]
		public void Delete()
		{
			// Arrange
			ValuesController controller = new ValuesController();

			// Act
			controller.Delete(5);

			// Assert
		}
	}
}
