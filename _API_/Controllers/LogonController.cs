﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static nilnul._acc_._API_.Controllers.RegController;

namespace nilnul._acc_._API_.Controllers
{
    public class LogonController : ApiController
    {
        // GET: api/Reg
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Reg/5
        public string Get(int id)
        {
            return "value";
        }

        // only one par is allowed in an http method;
        public Guid? _Post(string name, string pass)
        {
			return nilnul.acc._LogonX.Logon(name, pass);
        }

        // POST: api/Reg
		public Guid? Post([FromBody] PostPars postPars)
		{
			return _Post(postPars.name, postPars.pass);
		}


		// PUT: api/Reg/5
		public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Reg/5
        public void Delete(int id)
        {
        }
    }
}
