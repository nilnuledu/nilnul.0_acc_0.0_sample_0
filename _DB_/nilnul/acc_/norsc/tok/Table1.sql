﻿CREATE TABLE [nilnul.acc_.norsc].[Tok] --token; in session, or in cookie
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[acc] bigint not null references [nilnul.acc_].[Norsc](id)
	,
	[tok] uniqueidentifier not null default newid()
	,
	[due] datetime default DateAdd(minute,30, getUtcdate())
	,
	_time datetime default getUtcDate() -- not getDate()
	,
	_memo nvarchar(max) 
)
