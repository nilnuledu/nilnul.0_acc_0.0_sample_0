﻿CREATE PROCEDURE [nilnul.acc_.norsc.tok].[Logon]
	@name nvarchar(400),
	@pass nvarchar(400),
	@ret uniqueidentifier out
AS

declare @acc bigint=(
	select 
		top 1
			id 
			from 
				[nilnul.acc_].[Norsc]
			where 
				[name] = @name
				and
				[pass_hashed]= HASHBYTES(
					'sha2_512'
					,
					cast(@pass as varbinary(400)) + hash_salt
				)
);
--declare @ret uniqueIdentifier;

if (@acc is null) begin
	set @ret =null
end
else begin
	declare @tok table( tok uniqueIdentifier );

	insert [nilnul.acc_.norsc].Tok
		( acc)
		output inserted.tok into @tok
		--output inserted.*
		values(
			@acc

		)
	;
	set @ret= (select top 1 * from @tok );
end
--select @ret;
RETURN 0
