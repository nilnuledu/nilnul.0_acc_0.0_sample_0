﻿declare @pass nvarchar(400);
set @pass = N'zs123A!2234)';

declare @salt binary(64);
set @salt= CRYPT_GEN_RANDOM(
	64
	--,
	--0x01234567		--The length of seed must match the value of the length argument. The seed argument has a varbinary(8000) data type.
);
insert [nilnul.acc_].[Norsc]
	(
		[name]
		,
		[hash_salt]
		,
		[pass_hashed] 
	)
	output inserted.*
	values(
		N'zs'
		,
		@salt
			,
		HASHBYTES(
			'sha2_512'
			,
			cast(@pass as varbinary(400)) +@salt
		)	--binary will be converted to string, returned varbinary
	)

