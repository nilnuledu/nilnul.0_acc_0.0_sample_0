﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace nilnul._acc_._API_.Controllers
{
    public class RegController : ApiController
    {
        // GET: api/Reg
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Reg/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Reg
   //     public void Post(string name, string pass)
   //     {
			//nilnul.acc._RegX.Reg(name, pass);
   //     }
        public void Post([FromBody] PostPars postPars)
        {
			nilnul.acc._RegX.Reg(postPars.name, postPars.pass);
        }

		public class PostPars
		{
			public string name { get; set; }
			public string pass { get; set; }

			/// <summary>
			/// we need this to deserialize data into this model;
			/// </summary>
			public PostPars()
			{

			}

			public PostPars(string v1, string v2)
			{
				this.name = v1;
				this.pass = v2;
			}
		}

        // PUT: api/Reg/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Reg/5
        public void Delete(int id)
        {
        }
    }
}
