﻿CREATE PROCEDURE [nilnul.acc_.norsc].[Reg]
	@name nvarchar(400),
	@pass nvarchar(400)
AS


declare @salt binary(64);
set @salt= CRYPT_GEN_RANDOM(
	64
	--,
	--0x01234567		--The length of seed must match the value of the length argument. The seed argument has a varbinary(8000) data type.
);
insert [nilnul.acc_].[Norsc]
	(
		[name]
		,
		[hash_salt]
		,
		[pass_hashed] 
	)
	output inserted.*
	values(
		@name
		,
		@salt
			,
		HASHBYTES(
			'sha2_512'
			,
			cast(@pass as varbinary(400)) +@salt
		)	--binary will be converted to string, returned varbinary
	)

RETURN 0
