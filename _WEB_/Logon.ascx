﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Logon.ascx.cs" Inherits="nilnul._acc_._WEB_.Logon" %>
<script src="//code.jquery.com/jquery-3.6.3.min.js"></script>
<form>
	<input type="text" name="name" id="name"/>
	<input type="password" name="pass" id="pass"/>
	<input type="submit" value="Logon" />
</form>
<script>
	$("form").submit(
		function (e) {
			e.preventDefault();
			$.post(
				"/nilnul._acc_._API_/api/Logon"
				,
				{
					name: document.getElementById("name").value,
					pass: document.getElementById("pass").value
				}
				,
				function (t) {
					alert(t);
					document.cookie = t;
				}

			);
		}
	);
</script>
